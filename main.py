# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 00:03:27 2020

@author: T.Z.
"""
#from tensorflow.python import keras
import argparse
from glugan import GluGAN
from data.preprocess import load_data
import numpy as np

#%% Parametes of subjects
args = argparse.ArgumentParser()    
args.seq_len_in_minutes = 90
args.data_dir = 'Ohio_2020'
args.pid_list = ['540', '544', '552', '567', '584', '596']
args.data_files = ['./data/'+args.data_dir + '/Training/' + pID + '-ws-training.xml' for pID in args.pid_list]
args.data_files_test = ['./data/'+args.data_dir + '/Testing/'  + pID + '-ws-testing.xml' for pID in args.pid_list]
args.reg = False
args.seq_len = 18  # 90minutes
args.pred_horizon = 6 # 6 for 30 miuntes, 12 for 60 minutes
args.syn_length = args.pred_horizon
args.cats_to_load = ['bgl', 'meal', 'bolus']
args.label_differencing = True
args.mask_interpolated = True
args.batch_size = 512
args.bgl_scale = 0.01
args.scale_for_others = 0.3
args.normalize = True
args.data_resolution = 5
# Load data
data = load_data(args)

    
   
#%% Experiments

for pid in args.pid_list:
    N, T, D = data[pid]['train'].shape
    data_X = data[pid]['train']
    data_X_test = data[pid]['test']
    data_X_validate = data[pid]['validate']
    data_y = data[pid]['trainLabels']
    data_y_test = data[pid]['testLabels']
    data_y_validate = data[pid]['validateLabels']
    data_y_syn = data[pid]['trainSynLabels']
    data_y_syn_validate = np.expand_dims(data[pid]['validateSynLabels'],axis=2)
    data_y_syn_test = np.expand_dims(data[pid]['testSynLabels'],axis=2)
    #%% Create GluGAN
    
    params = dict()
       
    # Hyperparams can be tuned
    params['seq_Len'] = args.seq_len
    params['drnn_cell'] = 'GRU'
    params['hidden_dim'] = [32]*3
    params['num_layers'] = 3
    params['batch_size'] = args.batch_size
    params['syn_length'] = args.syn_length
    params['bgl_scale'] = args.bgl_scale
    params['annealing_step'] = 100
    params['dilation'] = [1,2,4]
    params['lam_1'] = 0.2
    params['generator learning rate'] = 0.0001
    params['discriminator learning rate'] = 0.0001
    params['num_epoch'] = 3000
    params['patience']  = 50
  

#     Create GAN 
    GAN = GluGAN(data_X, data_y,data_y_syn,data_X_validate,data_y_validate,data_y_syn_validate,params,False)
    GAN.predictive_training()
    y_hat, rmse_pred = GAN.predictive_testing(data_X_test, data_y_test, data_y_syn_test)


