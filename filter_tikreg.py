# -*- coding: utf-8 -*-
import numpy as np

def tikReg(labeMatr,lam = 3000):
    N = len(labeMatr)
    U_d = np.tri(N, N, 0)
    L_d = np.eye(N, k=-1) + np.eye(N, k=1) + np.eye(N, k=0) * (-2)
    labeMatr_hat = np.dot(np.linalg.inv(np.dot(U_d.T, U_d) + lam * np.dot(L_d.T, L_d)), np.dot(np.dot(U_d.T, U_d), labeMatr))
    
    return labeMatr_hat

def filter_outlier(data,args):
    lam = 1000
    len_seg = 288
    len_ori = 16
    dataRaw_train_reg = {}
    for j in range(len(args.pid_list)):
        print("\n> Regularization for patient '{}'...".format(args.pid_list[j]))
        BG_reg_list = []
        BG_seq = data[args.pid_list[j]][:,0]
        num_seg = np.int(len(BG_seq)/len_seg)
        for i in range(num_seg-1):
            BG_seq_ = BG_seq[i*len_seg:(i+1)*len_seg]
            BG_reg = tikReg(BG_seq_,lam)
            BG_ = np.concatenate((BG_seq_[0:len_ori],BG_reg[len_ori:-len_ori],BG_seq_[-len_ori:]))
            BG_reg_list.append(BG_)
        BG_seq_ = BG_seq[(num_seg-1)*len_seg:]
        BG_reg = tikReg(BG_seq_,lam)
        BG_ = np.concatenate((BG_seq_[0:len_ori],BG_reg[len_ori:-len_ori],BG_seq_[-len_ori:]))
        BG_reg_list.append(BG_)
        BG_reg_whole = np.concatenate(BG_reg_list)
        filtered_data = np.append(np.expand_dims(BG_reg_whole,axis=1),data[args.pid_list[j]][:,1:],axis = 1) 
        dataRaw_train_reg[args.pid_list[j]] = filtered_data
    
    
    return dataRaw_train_reg