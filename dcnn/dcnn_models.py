from dcnn import conv1d, dilated_conv1d


def dcnn_discriminator(
             inputs,
             num_time_samples,
             num_channels=1,
             num_classes=2,
             num_blocks=1,
             num_layers=3, # 6
             num_hidden=32,
             gpu_fraction=1.0):
    

    h = inputs
    hs = []
    for b in range(num_blocks):
        for i in range(num_layers):
            rate = 2**i
            name = 'block_{}-lalyer_{}'.format(b, i)
            h = dilated_conv1d(h, num_time_samples, num_hidden, rate=rate, name=name)
            hs.append(h)

    outputs = conv1d(h,
                     num_classes,
                     filter_width=1,
                     gain=1.0,
                     activation=None,
                     bias=True)
    return outputs




                
