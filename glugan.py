# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 22:53:54 2020

@author: T.Z.
"""


#%% Import
import tensorflow as tf
tf.debugging.set_log_device_placement(True)
tf.reset_default_graph()    
import numpy as np
import sys 
sys.path.append('./drnn')
sys.path.append('./dcnn')
from drnn_models import drnn_generator
from dcnn_models import dcnn_discriminator
from time import gmtime, strftime

#%% Main class
class GluGAN:
    def __init__(self, data_X, data_y,data_y_syn, X_validate, y_validate,y_syn_validate ,params,record_tb = True):

    
        # Data 
        self.data_dim = len(data_X[0][0,:])
        self.data_X = data_X
        self.data_y = data_y
        self.data_y_syn = data_y_syn
        self.No = len(data_X)
        self.X_validate = X_validate
        self.y_validate = y_validate
        self.y_syn_validate =  y_syn_validate
        
        # Parameters
        self.hidden_dim   = params['hidden_dim'] 
        self.num_layers   = params['num_layers']        
        self.batch_size   = params['batch_size']
        self.drnn_cell = params['drnn_cell']
        self.seq_Len  = params['seq_Len']        
        self.y_syn_len    = params['syn_length']
        self.bgl_scale    = params['bgl_scale']
        self.annealing_step = params['annealing_step']
        self.dialtion     = params['dilation']
        self.lam_1_inital        = params['lam_1']
        self.glr = params['generator learning rate']
        self.dlr = params['discriminator learning rate']
        self.num_epoch = params['num_epoch']
        self.patience = params['patience']
        self.iterations = int(self.num_epoch/self.batch_size)
        self.time  = strftime("%Y-%m-%d-%H-%M-%S", gmtime())
        self.record_tb = record_tb
        self.syn_restore = True
            
 
        # Placeholders
        self.X = tf.placeholder(tf.float32, [None, self.seq_Len, self.data_dim], name = "input")
        self.y = tf.placeholder(tf.float32, [None,1], name = "target")
        self.y_syn = tf.placeholder(tf.float32, [None,self.y_syn_len,1], name = "sync_target")
        self.lam_1 = tf.placeholder(tf.float32, shape=(None), name = "lam_1")
        self.lam_2 = 1 - self.lam_1
    

        # Generator
        self.y_hat = self.generator(self.X)
        self.y_syn_hat = tf.concat([self.y_syn[:,:-1,:], tf.expand_dims(self.y_hat,axis=2)],axis = 1)

   
        # Discriminator
        if self.syn_restore:
            y_fake = self.discriminator(self.y_syn_hat+tf.expand_dims(self.X[:,-self.y_syn_len:,0],axis=2))
            y_real = self.discriminator(self.y_syn+tf.expand_dims(self.X[:,-self.y_syn_len:,0],axis=2))
        else:
            y_fake = self.discriminator(self.y_syn_hat)
            y_real = self.discriminator(self.y_syn)     

        
        # Variables      
        gvars = [v for v in tf.trainable_variables() if v.name.startswith('generator')]
        dvars = [v for v in tf.trainable_variables() if v.name.startswith('discriminator')]
        
        # Generator loss
        self.G_loss = tf.losses.sigmoid_cross_entropy(tf.ones_like(y_fake), y_fake)
        
        # Discriminator loss
        D_loss_real = tf.losses.sigmoid_cross_entropy(tf.ones_like(y_real), y_real)
        D_loss_fake = tf.losses.sigmoid_cross_entropy(tf.zeros_like(y_fake), y_fake)
        
        # Prediction loss
        self.PD_loss = D_loss_real + D_loss_fake 
        self.MSE_loss = tf.losses.mean_squared_error(self.y, self.y_hat)
        self.PG_loss = self.lam_1*self.G_loss+self.lam_2*self.MSE_loss
        
        tf.summary.scalar("PD_loss", self.PD_loss)
        tf.summary.scalar("PG_loss", self.PG_loss)
        tf.summary.scalar("MSE_loss", self.MSE_loss)
        
        # Adam optimizers
        self.G_solver = tf.train.AdamOptimizer(learning_rate = self.glr).minimize(self.PG_loss, var_list = gvars)          
        self.D_solver = tf.train.AdamOptimizer(learning_rate = self.dlr).minimize(self.PD_loss, var_list = dvars)
        
        # Initialization  
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        
        # Tensorboard config
        self.merged = tf.summary.merge_all()
        if self.record_tb:
            self.train_writer = tf.summary.FileWriter( './tb_logs/'+self.time, self.sess.graph)
            self.train_writer.add_graph(self.sess.graph)
            
        # Load weights
        self.saver = tf.train.Saver(max_to_keep=50)
        checkpoint = tf.train.get_checkpoint_state('./weights/')
        if checkpoint and checkpoint.model_checkpoint_path:
                self.saver.restore(self.sess, checkpoint.model_checkpoint_path)
                self.path = checkpoint.model_checkpoint_path
                print ("Successfully loaded:", checkpoint.model_checkpoint_path)
        else:
                print ("Could not find old network weights")
  

        
    # build generator
    
    def generator (self, x):      
      
        with tf.variable_scope("generator", reuse = tf.AUTO_REUSE): 
            cell_type = self.drnn_cell
            hidden_structs = self.hidden_dim
            dilations = self.dialtion 
            dropout = 0
            y_dim = 1
            y_tilde = drnn_generator(x, hidden_structs, dilations, self.seq_Len ,y_dim, self.data_dim, dropout, cell_type)

        return y_tilde
              
    # build discriminator 
    
    def discriminator (self, x):
      
        with tf.variable_scope("discriminator", reuse = tf.AUTO_REUSE):

                y_hat = dcnn_discriminator(x, self.y_syn_len,num_layers=3) 
                    
        return y_hat   
    
    
    # Predictive Testing
    
    def predictive_testing(self,X_test,y_test,y_syn_test):
        y_hat =  self.sess.run([self.y_hat], feed_dict={self.X: X_test,self.y:y_test,self.y_syn:y_syn_test}) 
        rmse_pred = np.sqrt(np.mean((y_hat - y_test)**2))/self.bgl_scale
    
        
        return y_hat, rmse_pred
    
    # Predictive Tranining
            
    def predictive_training(self):
        print('Start Predictive Training')
        step = 0
        best_rmse, best_val_epoch = None, None
        lam_1 = self.lam_1_inital
        delta = self.lam_1_inital/self.annealing_step
        
        for cur_epoch in range(self.num_epoch):

            for local_step in range(int(self.No/self.batch_size)):
 
                # Anneal the weight of Discriminator loss            
                if step >= self.annealing_step:
                    lam_1 = 0
                else:
                    lam_1 = lam_1-delta
            
                # Generator Training               
                idx = np.random.permutation(self.No)
                train_idx = idx[:self.batch_size]               
                batch_x = list(self.data_X[i] for i in train_idx)
                batch_y = list(self.data_y[i] for i in train_idx)
                batch_y_syn = np.expand_dims(np.array([self.data_y_syn[i] for i in train_idx]),axis = 2)
                
    
                _, summary = self.sess.run([self.G_solver,self.merged],
                                                                       feed_dict={self.X: batch_x,
                                                                                  self.y:batch_y,
                                                                                  self.y_syn:batch_y_syn,
                                                                                  self.lam_1:lam_1})
    
                # Discriminator Training
                idx = np.random.permutation(self.No)
                train_idx = idx[:self.batch_size]              
                batch_x = list(self.data_X[i] for i in train_idx)
                batch_y = list(self.data_y[i] for i in train_idx)
                batch_y_syn = np.expand_dims(np.array([self.data_y_syn[i] for i in train_idx]),axis = 2)

                if lam_1>0:        
                    _, summary = self.sess.run([self.D_solver, self.merged], 
                                                            feed_dict={self.X: batch_x, 
                                                                       self.y:batch_y,
                                                                       self.y_syn:batch_y_syn,
                                                                       self.lam_1:lam_1})            
                if self.record_tb:
                    self.train_writer.add_summary(summary, step)
                
                step += 1
                
            # Validation        
            y_hat, rmse_pred  = self.predictive_testing(self.X_validate,self.y_validate,self.y_syn_validate)
            print('Current epoch: '+str(cur_epoch)+' Validation RMSE: '+str(np.round(rmse_pred,4)))
            
            # Ealry stop 
            if best_rmse is None or rmse_pred<best_rmse:
                best_rmse, best_val_epoch = rmse_pred, cur_epoch
              
            if best_val_epoch < cur_epoch - self.patience:
                print('Best epoch: '+str(best_val_epoch))    
                break              

                
                
        print('Finish Predictive Training')  
        

