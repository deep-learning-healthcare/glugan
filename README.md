

### Introduction: ###
Blood Glucose Level Prediction Challenge in the Knowledge Discovery in Healthcare Data (KDH) workshop, 2020

* Blood Glucose Prediction for Type 1 Diabetes Using Generative Adversarial Networks

* Author: Taiyu Zhu, Xi Yao, Kezhi Li, Pau Herrero and Pantelis Georgiou

* Abstrct: For people living with Type 1 diabetes (T1D), it is essential to maintain blood glucose (BG) levels in a target range. Accurate BG prediction is needed to reduce the risk of hypoglycemia and hyperglycemia and enhance early interventions. However, due to the complexity involved in glucose metabolism and various lifestyle events which can disrupt this, diabetes management remains challenging. In this work, we propose a novel deep learning model to predict future BG levels, based on the historical continuous glucose monitoring measurements, meal ingestion, and insulin delivery. We adopt a modified architecture of the generative adversarial network that comprises of a generator and a discriminator. The generator computes the BG predictions by a recurrent neural network with gated recurrent units, and the auxiliary discriminator employs a one-dimensional convolutional neural network to distinguish between the predictive and real BG values. Two modules are trained in an adversarial process with a combination of loss. 
The experiments were conducted on the OhioT1DM dataset that contains the data of six T1D contributors over 40 days. The proposed algorithm achieves an average root mean square error (RMSE) of 18.34 mg/dL with a mean absolute error (MAE) of 13.37 mg/dL for the 30-minute prediction horizon (PH) and an average RMSE of 32.31 mg/dL with a MAE of 24.20 for the 60-minute PH. The Clarke error grid, used to compare the data and results, confirms the promising clinical performance of the proposed model.


############################################################################

### Instructions: ###

* main.py starts experiments.
* The parameters of the subjects and listed hyperparameters can be tuned to obtain personalized models.
* Experiments will stop when the early stop criteria are met.
* glugan.py builds the deep learning graph.
* dcnn folder and drnn folder contain the modules of deep neural networks.

* Note that we did not upload the data folder, considering the privacy of data contributors and data use purposes.
* The dataset XML files and loading scripts are available (with DUA) at: http://smarthealth.cs.ohio.edu/nih.html.

############################################################################

### Relative Papers: ###

* Marling, C. and Bunescu, R., The OhioT1DM Dataset for Blood Glucose Level Prediction: Update 2020.
* Mirshekarian, S., Shen, H., Bunescu, R. and Marling, C., 2019, July. LSTMs and neural attention models for blood glucose prediction: Comparative experiments on real and synthetic data. In 2019 41st Annual International Conference of the IEEE Engineering in Medicine and Biology Society (EMBC) (pp. 706-712). IEEE.
* Chang, S., Zhang, Y., Han, W., Yu, M., Guo, X., Tan, W., Cui, X., Witbrock, M., Hasegawa-Johnson, M.A. and Huang, T.S., 2017. Dilated recurrent neural networks. In Advances in Neural Information Processing Systems (pp. 77-87).
* Paine, T.L., Khorrami, P., Chang, S., Zhang, Y., Ramachandran, P., Hasegawa-Johnson, M.A. and Huang, T.S., 2016. Fast wavenet generation algorithm. arXiv preprint arXiv:1611.09482.

